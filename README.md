                                                                   
# Smart Car with Ultrasonic Sensor

## Overview

This project implements an autonomous smart car using Arduino, an ultrasonic sensor (HC-SR04), and motor drivers. The smart car is designed to navigate its environment, avoid obstacles, and make decisions based on real-time sensor data.
# Autonomous Car README

This repository contains code for an autonomous car project using Arduino. The car is equipped with ultrasonic sensors to detect obstacles and navigate accordingly.

## Components
- Arduino board
- Ultrasonic sensor (HC-SR04)
- Motor driver module
- DC motors
- Power source (battery or external power supply)

## Pin Configuration
- TRIG_PIN: Pin connected to the trigger pin of the ultrasonic sensor (Digital Pin 2)
- ECHO_PIN: Pin connected to the echo pin of the ultrasonic sensor (Digital Pin 3)
- ENA: Enable A pin of the motor driver module (PWM Pin 5)
- ENB: Enable B pin of the motor driver module (PWM Pin 11)
- MOTOR_PWM_1, MOTOR_PWM_2, MOTOR_PWM_3, MOTOR_PWM_4: Pins controlling the motor driver module (Digital Pins 4, 10, 6, 7 respectively)

## Code Explanation
The code utilizes the ultrasonic sensor to detect obstacles and controls the movement of the car accordingly. Here's a brief explanation of the main functions:

- `readHC_SR04`: Reads the distance from the ultrasonic sensor.
- `controlMotors`: Controls the movement of the motors based on the direction provided.
- `getDirectionWithMaxDistance`: Determines the direction with the maximum distance using the ultrasonic sensor.
- `controlMotorsBasedOnDistance`: Adjusts the motor speed based on the distance from obstacles.

The `setup()` function initializes the serial communication, sets the pin modes, and initializes the motors. The `loop()` function continuously reads the distance, adjusts the motor speed, and controls the car's movement accordingly.

## Usage
1. Connect the Arduino board to the components as per the pin configuration mentioned.
2. Upload the code to the Arduino board.
3. Power the system.
4. The car should now autonomously navigate based on the detected obstacles.


Happy experimenting! 🚗💨


## How to Use

1. Connect the hardware components based on the provided pin configuration.
2. Upload the Arduino sketch (`smart_car.ino`) to the Arduino board.
3. Power up the smart car and observe its behavior in response to obstacles.

## Functionalities

- **Control Motors:** The `controlMotors` function takes a direction and motor speed as input to move the car in the specified direction.

- **Read Ultrasonic Sensor:** The `readHC_SR04` function reads the distance from the ultrasonic sensor and returns a status (OK, TIMEOUT, or ERROR).

- **Get Direction with Max Distance:** The `getDirectionWithMaxDistance` function measures distances in each direction and determines the direction with the maximum distance.

- **Control Motors Based on Distance:** The `controlMotorsBasedOnDistance` function gradually accelerates the motor speed based on a predefined acceleration time.

- **Main Loop:** The `loop` function continuously monitors the ultrasonic sensor readings, adjusts motor speed, and changes direction if obstacles are detected.

## Customization

- Adjust constants such as `ACCELERATION_TIME`, `SPEED_MOTOR`, `TIME_SPEED`, and others in the code to customize the smart car's behavior.

## Note
- Ensure proper power supply to the motors and the Arduino board.
- Adjust the constants like `TIME_DELAY`, `MEASURE_INTERVAL`, `ACCELERATION_TIME`, `base_speed`, `SPEED_MOTOR`, and `TIME_SPEED` as per your requirements.
- Fine-tune the motor speeds and control logic for optimal performance.

## Contributors

- **Author:** chaturong
- **Email:** ่jaturong.elec@gmail.com

Feel free to reach out for assistance or to report any issues. Happy exploring!