#define MEASURE_INTERVAL 100

#define ENA 5  //enable A on pin 5 (needs to be a pwm pin)
#define ENB 11  //enable B on pin 3 (needs to be a pwm pin)
#define MOTOR_PWM_1 4  //IN1 on pin 2 conrtols one side of bridge A
#define MOTOR_PWM_2 10  //IN2 on pin 4 controls other side of A
#define MOTOR_PWM_3 6  //IN3 on pin 6 conrtols one side of bridge B
#define MOTOR_PWM_4 7  //IN4 on pin 7 controls other side of B


unsigned long previousMillis = 0;
unsigned long startAccelerationMillis = 0;
  
int16_t TIME_DELAY = 1000;
uint8_t control_speed_motor = 50;

#define ACCELERATION_TIME 1000  // Time in milliseconds for acceleration
uint8_t base_speed = 65;   // value to start motor
int8_t SPEED_MOTOR = 100;   //max vlaue motor
int8_t TIME_SPEED = 10;  // time for + speed motor

#define TRIG_PIN 2  // You can use any available digital pin
#define ECHO_PIN 3  // You can use any available digital pin

enum HCSR04Status {
  HCSR04_OK,
  HCSR04_TIMEOUT,
  HCSR04_ERROR
};

enum MotorDirection {
  FRONT,
  BACK,
  LEFT,
  RIGHT,
  STOP
};
